const express = require('express');
const mongoose = require("mongoose");
const userRoutes = require('./routes/user')
const app = express();

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });
mongoose.set('useCreateIndex', true);
mongoose.connect('mongodb+srv://SFZ:Mongotheo123@cluster0.yzy7w.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    { useNewUrlParser: true,
        useUnifiedTopology: true })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch(() => console.log('Connexion à MongoDB échouée !'));


app.use('api/auth', userRoutes);

// app.post('/api/stuff', (req, res, next) => {
//     const thing = new Thing({
//         ...req.body
//     });
//     thing.save()
//         .then(() => res.status(201).json({ message: 'Objet enregistré !'}))
//         .catch(error => res.status(400).json({ error }));
// });


// app.post('/login',
// passport.authenticate('local'),
// function(req, res) {
//   // If this function gets called, authentication was successful.
//   // `req.user` contains the authenticated user.
//   res.redirect('/users/' + req.user.username);
// });
// app.post('/login',
//     passport.authenticate('local', { successRedirect: '/',
//         failureRedirect: '/login' }));
//
// app.post('/api/stuff', (req, res) => {
//     console.log(req.body);
//     res.status(201).json({
//       message: 'Objet créé !'
//     });
//   });
//
// const mysql      = require('mysql');
// const connection = mysql.createConnection({
//   host     : 'localhost',
//   user     : 'root',
//   password : '',
//   database : 'xeatdb'
// });
//
// connection.connect();
//
// connection.query('SELECT * FROM orders', function(err, rows, fields) {
//   if (err){
//       console.log('Erreur')
//   }else{
//     // console.log('The solution is: ', rows);
//
//     app.get('/', (req, res) => {
//         res.json(rows);
//         })
//   }
// });
//
//
// app.use(bodyParser.urlencoded({ extended: true }));
// app.listen(80);
//
// app.post('/post.html', function(request, response) {
//   var p1 = request.body.p1;
//   console.log("status " + p1);
// });
// connection.end();
module.exports = app;

